package ceyhun.shapes;

public class Rectangle{
  public int width;
  public int height;

  public Rectangle(int w,int h){
    width = w;
    height = h;
  }

  public int Area(){
    return width * height;
  }

}
