public class Q5 {
	int a;
	public static void main(String[] args) {
		Q5 c= new Q5();
		c.a = 5;
		int a = 10;
		increment(c, a);
		System.out.println("c.a = " + c.a + ", a = " + a);
	}
	public static void increment(Q5 c, int a){
		c.a++;
		a++;
	}
}