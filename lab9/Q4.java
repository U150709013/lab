
public class Q4 {

	public static void main(String[] args){
		
		int[] primeNumbers = new int[50];
		int starter = 2;
		int index = 0;
		
		while(index < 50){

			int counter = 0;
			
			for(int i = 2 ; i < starter ; i++){
				
				if(starter % i == 0){
					
					counter++;
					
				}
				
			}
			
			if(counter == 0){
				primeNumbers[index] = starter;
				index++;
			}
			starter++;
		}
	
		for(int i=0; i< 50; i++){
			System.out.print(primeNumbers[i] + " ");
		}
	}
}
