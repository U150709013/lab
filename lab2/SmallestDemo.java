public class SmallestDemo {
    public static void main(String[] args){
        int value1 = Integer.parseInt(args[0]);
        int value2 = Integer.parseInt(args[1]);
        int value3 = Integer.parseInt(args[2]);
        int result;
        int num1;
        num1 = (value1 < value2) ? value1 : value2;
        result = (num1 < value3) ? num1 : value3;
        System.out.println(result);
  }
}
