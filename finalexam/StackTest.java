import lab.q11.Stack;
import lab.q11.StackList;


public class StackTest {
	
	public static void main(String[] args){
		Stack<String> stack = new StackList<>();
		stack.push("7");
		stack.push("6");
		stack.push("3");
		stack.push("1");
		
		System.out.println(stack.pop());
		System.out.println(stack.empty());
	}

}
