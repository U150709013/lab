package lab.q10;

import java.io.*;

public class GradeCalculator {
	
	public static void main(String[] args) throws IOException{
		
		GradeCalculator gc = new GradeCalculator();
		String file = "/Users/cmelek/Desktop/Workspace/lab/finalexam/lab/q10/scores.txt";
		try {
			gc.processFile(file);
		} catch (FileNotFoundException e) {
			System.out.println(file + " does not exist");
		}
	}

	private void processFile(String filename) throws IOException {

		File file = new File(filename);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		FileWriter writer = new FileWriter("/Users/cmelek/Desktop/Workspace/lab/finalexam/lab/q10/grades.txt");
		
		String line = reader.readLine();
		int count = 0;
		
		writer.write("Student ID" + "\t"+ "Score" + "\t" + "Grade" + "\n");
		while(line != null){
			count++;
			if(count > 1){
				String items[] = line.split("\t");
				String grade = Grade(Integer.parseInt(items[1]));
				writer.write(items[0] + "\t"+ items[1] + "\t" + grade +"\n");
			}
			line = reader.readLine();
		}
			
		reader.close();
		writer.close();
	}
	
	public String Grade(int g){
		if(g <= 100 && g >= 90){
			return "A";
		}if(g < 90 && g >= 80){
			return "B";
		}if(g < 80 && g >= 70){
			return "C";
		}if(g < 70 && g >= 60){
			return "D";
		}if(g < 60 && g >= 0){
			return "F";
		}
		throw new IllegalArgumentException("Score is greater than 100 or less than 0");
	}

}
