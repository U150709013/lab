package lab;

public class q6 {
	
	public static void main(String[] args){
		int[][] nums = new int[][]{
			  { 5, 0, 0, 5, 0, 0, 0, 0, 364, 0 },
			  { 0, 0, 0, 0, 0, 0, 0, 8, 0, 0 },
			  { 123, 0, 0, 0, 0, 3, 0, 0, 0, 0 },
			  { 0, 0, 0, 142, 0, 0, 0, 540, 0, 0 },
			  { 0, 0, 0, 0, 0, 43, 0, 0, 0, 0 }
			};
			
		System.out.println(greaterFinder(nums));
	}
	
	public static int greaterFinder(int a[][]){
		int greatest = 0;
		
		for(int b[] : a){
			for(int nums : b){
				if(nums > greatest){
					greatest = nums;
				}
			}
		}
		
		return greatest;
		
	}

}
