package lab.q9;

public class Cube extends Rectangle {
	
	private int side;

	public Cube(int side) {
		super(side, side);
		this.setSide(side);
	}

	public int getSide() {
		return super.getWidth();
	}

	public void setSide(int side) {
		super.setLength(side);
		super.setWidth(side);
		this.side = side;
	}
	
	public int area(){
		return 6 * super.area();
	}
	
	public int volume(){
		return super.area() * side;
	}
	
}
