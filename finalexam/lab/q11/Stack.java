package lab.q11;

public interface Stack<T> {
	
	public void push (T t);
	public String pop ();
	public boolean empty();
}
