package lab;

public class q1 {
	
	public static void main(String[] args){
		System.out.println(Grade(64));
	}
	
	public static String Grade(int g){
		if(g <= 100 && g >= 90){
			return "A";
		}if(g < 90 && g >= 80){
			return "B";
		}if(g < 80 && g >= 70){
			return "C";
		}if(g < 70 && g >= 60){
			return "D";
		}if(g < 60 && g >= 0){
			return "F";
		}
		throw new IllegalArgumentException("Score is greater than 100 or less than 0");
	}
	
}
