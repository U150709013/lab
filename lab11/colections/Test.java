package colections;

import java.util.*;

public class Test {

	public static void main(String[] args){
		
		String values = "23 4 6 12 5 4 16 12 6";
		String[] arr = values.split(" ");
		
		Collection<String> col = new ArrayList<>();
		
		for(String str: arr){
			
			col.add(str);
			
		}
		
		System.out.println(col);
		
		col.add("4");
		
		System.out.println(col);
		
		// HashSet Test
		
		Set<String> set = new HashSet();
		
		for(String str: arr){
			
			set.add(str);
			
		}
		
		System.out.println("HashSet = " + set);
		
		set.add("4");
		
		System.out.println("HashSet = " + set);
	
		
		// LinkedHashSet Test
		
		Set<String> linkedset = new LinkedHashSet();
				
		for(String str: arr){
					
			linkedset.add(str);
					
		}
				
		System.out.println("LinkedHashSet = " + linkedset);
				
		linkedset.add("4");
				
		System.out.println("LinkedHashSet = " + linkedset);
		

		// TreeSet Test default
		
		Set<String> treeset = new TreeSet();
				
		for(String str: arr){
					
			treeset.add(str);
					
		}
				
		System.out.println("TreeSet = " + treeset);
				
		treeset.add("15");
				
		System.out.println("TreeSet = " + treeset);
		

		// TreeSet Test with comparator
		
		Set<String> treeset2 = new TreeSet(new NumberComparator());
				
		for(String str: arr){
					
			treeset2.add(str);
					
		}
				
		System.out.println("TreeSet Comp = " + treeset2);
				
		treeset2.add("15");
				
		System.out.println("TreeSet Comp = " + treeset2);
		
		
		//Queue
		
		
		Queue<String> queue = new LinkedList<>();
		
		for(String str: arr){
			
			queue.add(str);
			
		}
		
		System.out.println("Queue = " + queue);
		
		System.out.println("removed = " +queue.remove());
		
		System.out.println("Queue = " +queue);
		
		
		
		//Priority Queue
		
		
		Queue<String> pqueue = new PriorityQueue<>();
		
		for(String str: arr){
			
			pqueue.add(str);
			
		}
		
		System.out.println("Queue = " + pqueue);
		
		System.out.println("removed = " +pqueue.remove());
		
		System.out.println("Queue = " +pqueue);
		
		
		
		
		//Priority Queue with comparator
		
		
		Queue<String> pcqueue = new PriorityQueue<>(new NumberComparator());
		
		for(String str: arr){
			
			pcqueue.add(str);
			
		}
		
		System.out.println("Queue with Comp = " + pcqueue);
		
		System.out.println("removed = " +pcqueue.remove());
		
		System.out.println("Queue With comp = " +pcqueue);
	
		
		
		//Priority Queue with comparator
		
		
		Queue stack = Collections.asLifoQueue(new LinkedList());
		
		for(String str: arr){
			
			stack.add(str);
			
		}
		
		System.out.println("Queue with Comp = " + stack);
		
		System.out.println("removed = " +stack.remove());
		
		System.out.println("Queue With comp = " +stack);
	

	
	}
}
