package ceyhun.main;
import ceyhun.shapes.*;
import java.util.ArrayList;

public class Main{
  public static void main(String[] args){

    Circle cr1 = new Circle(5);
    Rectangle rc1 = new Rectangle(5,6);

    System.out.println(cr1.Area());
    System.out.println(rc1.Area());

    ArrayList<Circle> circles = new ArrayList();
    circles.add(cr1);
    circles.add(new Circle(6));
    circles.add(new Circle(7));

    System.out.println();

    Drawing drawing = new Drawing();

    for (int i = 0; i < circles.size(); i++){
      Circle circ = circles.get(i);
      drawing.addCircle(circ);
    }

    drawing.printAreas();

  }
}
