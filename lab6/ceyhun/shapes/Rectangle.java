package ceyhun.shapes;

public class Rectangle{
  int width;
  int height;

  public Rectangle(int w,int h){
    width = w;
    height = h;
  }

  public int Area(){
    return width * height;
  }

}
