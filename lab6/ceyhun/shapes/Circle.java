
package ceyhun.shapes;
public class Circle{
  int radius;
  public Circle(int r){
    radius = r;
  }
  public double Area(){
    return Math.PI * radius * radius;
  }
}
